package uz.pdp.model;
//Sevinch Abdisattorova 12/20/2021 2:13 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class BotUser {
    private final UUID id = UUID.randomUUID();
    private String firstName;
    private String lastName;
    private int round;
    private long chatId;
    private int editedMsgId;
    private boolean isAdmin;
    private boolean isStarted;

    public BotUser(String firstName, String lastName, long chatId, boolean isAdmin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.chatId = chatId;
        this.isAdmin = isAdmin;
    }
}
