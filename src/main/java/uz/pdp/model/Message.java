package uz.pdp.model;
//Sevinch Abdisattorova 12/20/2021 2:14 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class Message {
    private BotUser sender;
    private LocalDateTime created_at = LocalDateTime.now();
    private Long chatId;
//    private int messageId;
    private String text;
    private boolean isRead;

    public Message(BotUser sender, Long chatId, String text) {
        this.sender = sender;
        this.chatId = chatId;
        this.text = text;
    }
}
