package uz.pdp.services;
//Sevinch Abdisattorova 12/20/2021 6:53 PM

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.Bot;
import uz.pdp.model.BotUser;
import uz.pdp.model.Message;
import uz.sevinch.MyUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static uz.sevinch.MyUtil.*;
import static uz.pdp.Db.*;

public class AdminServiceImpl implements AdminService {
    @Override
    public void showMenu() {
        println(BLUE_BOLD, "~~~~~~~~~~~~~~~ MENU ~~~~~~~~~~~~~~~");
        println(BLUE_BOLD, "1=> All users, 2=> Send message to user ");
        print(GREEN_BOLD, "Your option: ");
        int i = scnInt();
        switch (i) {
            case 1:
                showUsers();
                break;
            case 2:
                if (messagesMap.size() == 0) {
                    println(RED, "No chats yet!!!");
                    break;
                }
                showUsers();
                print(BLUE_BOLD, "Enter chat id: ");
                long chatId = scannerInt.nextLong();
                sendMessageToUser(chatId);
                break;
            default:
                println(RED,
                        "Error! Wrong input option.");
                break;
        }
        showMenu();
    }


    @Override
    public void showUsers() {
        if (messagesMap.size() == 0) {
            println(RED, "No users yet!!!");
            return;
        }
        messagesMap.values().forEach(messages -> {
                    Message message = messages.get(0);
                    long count = messages.stream().filter(message1 -> !message1.isRead()).count();
                    println(CYAN, "Chat id: " + message.getChatId() + "  " +
                            message.getSender().getFirstName() +
                            " " + (message.getSender().getLastName() != null ?
                            message.getSender().getLastName() : "") +
                            " (# " + count + ")");

                }
        );

    }

    @Override
    public void sendMessageToUser(long chatId) {
        showMessages(chatId);
        while (true) {
            print(BLUE_BOLD, "me :(0=>Back)  ");
            String text = MyUtil.scnStr();
            if (text.equals("0")) {
                return;
            }

            Message message = new Message(admin, LocalDateTime.now(), chatId, text, true);
            messagesMap.get(chatId).add(message);

            SendMessage sendMessage = new SendMessage();
            sendMessage.setText(text);
            sendMessage.setChatId(String.valueOf(chatId));
            Bot bot = new Bot();
            try {
                bot.execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

        }
    }

    private void showMessages(long chatId) {
        messagesMap.get(chatId).forEach(message -> {
            message.setRead(true);
            if (!message.getSender().isAdmin()) {
                println( BLUE_BOLD,
                        message.getSender().getFirstName() +
                                " " + (message.getSender().getLastName() != null ?
                                message.getSender().getLastName() : "")
                                + " :  " + message.getText() + "\t\t\t" +
                                message.getCreated_at().format(DateTimeFormatter.ofPattern("HH:mm"))
                );
            }
            else {
                println( GREEN_BOLD,
                        "me"
                                + " :  " + message.getText() + "\t\t\t" +
                                message.getCreated_at().format(DateTimeFormatter.ofPattern("HH:mm"))
                );
            }
        });
    }
}
