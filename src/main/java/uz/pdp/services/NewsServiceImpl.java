package uz.pdp.services;
//Sevinch Abdisattorova 12/20/2021 3:57 PM

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import uz.pdp.model.Author;
import uz.pdp.model.BotUser;
import uz.pdp.model.Post;
import static uz.pdp.Db.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import static uz.pdp.util.Constants.*;


public class NewsServiceImpl implements NewsService {
    @Override
    public InlineKeyboardMarkup newsInlineKeyboard(BotUser currentUser) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineKeyBtnList = new ArrayList<>();
        inlineKeyboardMarkup.setKeyboard(inlineKeyBtnList);
        List<InlineKeyboardButton> inLineBtnRow = new ArrayList<>();

        for (int i = 0; i < numberOfNewsInOnePage; i++) {
            InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
            inlineKeyboardButton.setText("" + (i + 1));
            inlineKeyboardButton.setCallbackData("" + (i + 1));
            inLineBtnRow.add(inlineKeyboardButton);
            if (i % 2 == 0) {
                inlineKeyBtnList.add(inLineBtnRow);
            } else {
                inLineBtnRow = new ArrayList<>();
            }
        }
        if (currentUser.getRound() != 0) {
            inLineBtnRow = new ArrayList<>();
            InlineKeyboardButton b = new InlineKeyboardButton("Previous ⏮");
            b.setCallbackData("prev");
            inLineBtnRow.add(b);
        }
        if (currentUser.getRound() != postList.size() / 10 - 1) {
            InlineKeyboardButton b2 = new InlineKeyboardButton("Next ⏭");
            b2.setCallbackData("next");
            inLineBtnRow.add(b2);
        }
        inlineKeyBtnList.add(inLineBtnRow);

        return inlineKeyboardMarkup;
    }

    @Override
    public String showNews(BotUser user) {
        int postsNumber = postList.size();
        int pages = postsNumber / numberOfNewsInOnePage;
        int round = user.getRound();
        List<Post> collect = postList.stream()
                .skip((long) round * numberOfNewsInOnePage).
                limit(numberOfNewsInOnePage).
                collect(Collectors.toList());
        String text = "~~~~~~~~~~~~~~~~ NEWS ~~~~~~~~~~~~~~~~\n" +
                 postsNumber + " news found!\t\t\t" + "[" + (round+1) + " of " + pages + "]\n\n";
        for (int i = 0; i < numberOfNewsInOnePage; i++) {
            Post post =collect.get(i);
            text += (i + 1)  + ". " + post.getTitle() + "\n";
        }
        return text;
    }

    @Override
    public String showNewsInfo(BotUser currentUser,String data) {
        int round = currentUser.getRound();
        int option = Integer.parseInt(data) - 1;
        int newsIndex = round * 10 + option;
        Post post = postList.get(newsIndex);
        Long userId = post.getUserId();
        Author author = authorList.stream().filter(user ->
                        user.getId() == userId).findFirst().
                orElse(null);
        return "\t\tInfo about author\n" +
                "name: " + author.getName() + "\n" +
                "email: " + author.getEmail() + "\n\n" +
                "Tittle: " + post.getTitle() + "\n\n" +
                "Body: " + post.getBody() + "\n";

    }
}
