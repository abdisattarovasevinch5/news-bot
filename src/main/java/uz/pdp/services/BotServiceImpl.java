package uz.pdp.services;
//Sevinch Abdisattorova 12/20/2021 10:44 PM

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.Bot;
import uz.pdp.model.BotUser;

import java.util.ArrayList;
import java.util.Collections;

import static uz.pdp.Db.*;
import static uz.pdp.Db.messagesMap;
import static uz.sevinch.MyUtil.*;

public class BotServiceImpl implements BotService {
    @Override
    public BotUser getCurrentUser(Long chatId, Message message) {
        if (userMap.containsKey(chatId)) {
            return userMap.get(chatId);
        } else {
            User from = message.getFrom();
            BotUser botUser = new BotUser(
                    from.getFirstName(),
                    from.getLastName(),
                    0,
                    chatId,
                    0,
                    false,
                    false
            );
            userMap.put(chatId, botUser);
            return botUser;
        }
    }

    @Override
    public void updateHasMessage(Update update) {
        Bot bot = new Bot();
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.enableNotification();
        Message messageFromUser = update.getMessage();
        long chatId = messageFromUser.getChatId();
        BotUser currentUser = getCurrentUser(chatId, messageFromUser);
        String text = messageFromUser.getText();
        sendMessage.setChatId(String.valueOf(chatId));

        String lastName = messageFromUser.getFrom().getLastName() != null ?
                messageFromUser.getFrom().getLastName() : "";
        messageFromUser.getFrom().getFirstName();
        String firstName = messageFromUser.getFrom().getFirstName();
        switch (text) {
            case "/start":
                currentUser.setEditedMsgId(0);
                currentUser.setStarted(true);
                String s = newsService.showNews(currentUser);
                sendMessage.setText("Hello, " +
                        firstName + " " + lastName + "\n" + s);
                sendMessage.setReplyMarkup(newsService.newsInlineKeyboard(currentUser));
                break;
            default:
                if (!currentUser.isStarted()) {
                    sendMessage.setText("Please /start first!");
                    sendMessage.getText();
                    try {
                        currentUser.setEditedMsgId(bot.execute(sendMessage).getMessageId());
                        Thread.sleep(5000);
                        DeleteMessage deleteMessage = new DeleteMessage(String.valueOf(chatId),
                                currentUser.getEditedMsgId());
                        bot.execute(deleteMessage);
                        return;
                    } catch (TelegramApiException | InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    uz.pdp.model.Message message = new uz.pdp.model.Message(currentUser, chatId, text);
                    if (messagesMap.containsKey(chatId)) {
                        messagesMap.get(chatId).add(message);
                    } else {
                        messagesMap.put(chatId, new ArrayList<>(Collections.singletonList(message)));

                    }
                    return;

                }


        }
        try {
            bot.execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateHasCallBackQuery(Update update) {
        Bot bot = new Bot();
        SendMessage sendMessage = new SendMessage();

        CallbackQuery callbackQuery = update.getCallbackQuery();
        String data = callbackQuery.getData();

        long chatId1 = callbackQuery.getMessage().getChatId();
        long msgId = callbackQuery.getMessage().getMessageId();
        String inlineMessageId = callbackQuery.getInlineMessageId();
        BotUser currentUser = userMap.get(chatId1);
        sendMessage.setChatId(String.valueOf(chatId1));

        EditMessageText new_message = new EditMessageText();
        new_message.setChatId(String.valueOf(chatId1));
        new_message.setMessageId((int) msgId);
        new_message.setInlineMessageId(inlineMessageId);

        switch (data) {
            case "prev":
                currentUser.setRound(currentUser.getRound() - 1);
                new_message.setText(newsService.showNews(currentUser));
                new_message.setReplyMarkup(newsService.newsInlineKeyboard(currentUser));
                break;
            case "next":
                currentUser.setRound(currentUser.getRound() + 1);
                new_message.setText(newsService.showNews(currentUser));
                new_message.setReplyMarkup(newsService.newsInlineKeyboard(currentUser));
                break;
            default:
                if (currentUser.getEditedMsgId() != 0) {
                    EditMessageText new_message1 = new EditMessageText();
                    new_message1.setChatId(String.valueOf(chatId1));
                    new_message1.setMessageId((int) currentUser.getEditedMsgId());
                    new_message1.setText(newsService.showNewsInfo(currentUser, data));
                    try {
                        bot.execute(new_message1);
                        return;
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else {
                    sendMessage.setText(newsService.showNewsInfo(currentUser, data));
                    sendMessage.getText();
                    try {
                        currentUser.setEditedMsgId(bot.execute(sendMessage).getMessageId());
                        return;
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
        }
        try {
            bot.execute(new_message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateMainService(Update update) {

        if (update.hasMessage()) {
            updateHasMessage(update);
        }
        if (update.hasCallbackQuery()) {
            updateHasCallBackQuery(update);
        }
    }
}
