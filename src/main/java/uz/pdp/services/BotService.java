package uz.pdp.services;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.model.BotUser;

public interface BotService {

    BotUser getCurrentUser(Long chatId, Message message);

    void updateHasMessage(Update update);
    void updateHasCallBackQuery(Update update);

    void updateMainService(Update update) ;
}
