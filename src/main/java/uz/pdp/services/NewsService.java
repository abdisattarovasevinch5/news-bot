package uz.pdp.services;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import uz.pdp.model.BotUser;

public interface NewsService {

    InlineKeyboardMarkup newsInlineKeyboard(BotUser currentUser);

    String showNews(BotUser user);

    String showNewsInfo(BotUser user, String data);
}
