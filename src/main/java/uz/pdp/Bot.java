package uz.pdp;
//Sevinch Abdisattorova 12/18/2021 8:56 AM

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.services.BotServiceImpl;

import static uz.pdp.util.Constants.botToken;
import static uz.pdp.util.Constants.botUsername;

public class Bot extends TelegramLongPollingBot {

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        BotServiceImpl botService = new BotServiceImpl();
        botService.updateMainService(update);


    }
}
//        if (update.hasMessage()) {
//            Message messageFromUser = update.getMessage();
//            chatId = messageFromUser.getChatId();
//            currentUser = getCurrentUser(chatId, messageFromUser);
//            String text = messageFromUser.getText();
//            sendMessage.setChatId(String.valueOf(chatId));
//
//            String lastName = messageFromUser.getFrom().getLastName() != null ?
//                    messageFromUser.getFrom().getLastName() : "";
//            messageFromUser.getFrom().getFirstName();
//            String firstName = messageFromUser.getFrom().getFirstName();
//            switch (text) {
//                case "/start":
//                    currentUser.setStarted(true);
//                    String s = newsService.showNews(currentUser);
//                    sendMessage.setText("Hello, " +
//                            firstName + " " + lastName + "\n" + s);
//                    sendMessage.setReplyMarkup(newsService.newsInlineKeyboard(currentUser));
//                    break;
//                default:
//                    if (!currentUser.isStarted()) {
//                        sendMessage.setText("Please /start first!");
//                        sendMessage.getText();
//                        try {
//                            currentUser.setEditedMsgId(execute(sendMessage).getMessageId());
//                            Thread.sleep(5000);
//                            DeleteMessage deleteMessage = new DeleteMessage(String.valueOf(chatId),
//                                    currentUser.getEditedMsgId());
//                            execute(deleteMessage);
//                            return;
//                        } catch (TelegramApiException | InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        uz.pdp.model.Message message = new uz.pdp.model.Message(currentUser, chatId, text);
//                        if (messagesMap.containsKey(chatId)) {
//                            messagesMap.get(chatId).add(message);
//                        } else {
//                            messagesMap.put(chatId, new ArrayList<>(Collections.singletonList(message)));
//
//                        }
//                        return;
//
//                    }
//
//
//            }
//            try {
//                execute(sendMessage);
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//
//        }

//            CallbackQuery callbackQuery = update.getCallbackQuery();
//            String data = callbackQuery.getData();
//
//            long chatId1 = callbackQuery.getMessage().getChatId();
//            long msgId = callbackQuery.getMessage().getMessageId();
//            String inlineMessageId = callbackQuery.getInlineMessageId();
//            currentUser = userMap.get(chatId1);
//            sendMessage.setChatId(String.valueOf(chatId1));
//
//            EditMessageText new_message = new EditMessageText();
//            new_message.setChatId(String.valueOf(chatId1));
//            new_message.setMessageId((int) msgId);
//            new_message.setInlineMessageId(inlineMessageId);
//
//            switch (data) {
//                case "prev":
//                    currentUser.setRound(currentUser.getRound() - 1);
//                    new_message.setText(newsService.showNews(currentUser));
//                    new_message.setReplyMarkup(newsService.newsInlineKeyboard(currentUser));
//                    break;
//                case "next":
//                    currentUser.setRound(currentUser.getRound() + 1);
//                    new_message.setText(newsService.showNews(currentUser));
//                    new_message.setReplyMarkup(newsService.newsInlineKeyboard(currentUser));
//                    break;
//                default:
//                    if (currentUser.getEditedMsgId() != 0) {
//                        EditMessageText new_message1 = new EditMessageText();
//                        new_message1.setChatId(String.valueOf(chatId1));
//                        new_message1.setMessageId((int) currentUser.getEditedMsgId());
//                        new_message1.setText(newsService.showNewsInfo(currentUser, data));
//                        try {
//                            execute(new_message1);
//                            return;
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        sendMessage.setText(newsService.showNewsInfo(currentUser, data));
//                        sendMessage.getText();
//                        try {
//                            currentUser.setEditedMsgId(execute(sendMessage).getMessageId());
//                            return;
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    }
//            }
//            try {
//                execute(new_message);
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }