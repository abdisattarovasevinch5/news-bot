package uz.pdp;
//Sevinch Abdisattorova 12/18/2021 9:04 AM

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import uz.pdp.model.Author;
import uz.pdp.model.BotUser;
import uz.pdp.model.Message;
import uz.pdp.model.Post;
import uz.pdp.services.NewsServiceImpl;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Db {
    public static NewsServiceImpl newsService = new NewsServiceImpl();

    public static void allDataFromJson() {
        readAuthorsFromJson();
        readPostsFromJson();
    }

    public static BotUser admin = new BotUser("Admin", "", 1, true);
    public static Map<Long, BotUser> userMap = new HashMap<Long, BotUser>() {{
        put(1L, admin);
    }};


    public static List<Post> postList = new ArrayList<>();

    public static List<Author> authorList = new ArrayList<>();

    public static Map<Long, ArrayList<Message>> messagesMap = new HashMap<>();


    private static void readPostsFromJson() {
        try (Reader reader = new FileReader("src/main/resources/posts.json")) {
            Type arrayListTypeToken = new TypeToken<ArrayList<Post>>() {
            }.getType();
            List<Post> postsFromJson = new Gson().fromJson(reader, arrayListTypeToken);
            postList = postsFromJson;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readAuthorsFromJson() {
        try (Reader reader = new FileReader("src/main/resources/authors.json")) {
            Type arrayListTypeToken = new TypeToken<ArrayList<Author>>() {
            }.getType();
            List<Author> authorsFromJson = new Gson().fromJson(reader, arrayListTypeToken);
            authorList = authorsFromJson;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
