package uz.pdp;
//Sevinch Abdisattorova 12/18/2021 8:56 AM

import org.checkerframework.checker.units.qual.A;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.pdp.services.AdminServiceImpl;

import static uz.pdp.Db.*;
import static uz.sevinch.MyUtil.*;

public class Main {
    public static void main(String[] args) {
        allDataFromJson();

        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(new Bot());

            println(RED_BRIGHT, "Bot is started!!!");

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        AdminServiceImpl adminService = new AdminServiceImpl();
        adminService.showMenu();
    }
}
